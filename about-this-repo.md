This repo is not the official source of FermaT.

It is based on the official sources from gkc.org.uk and is maintaned
as part of the CLATU project (computer language automated transformation
utilities).

https://perun.pmf.uns.ac.rs/pracner/transformations/

Effort is invested into marking commits properly from the upstream sources
and to have a separate "upstream" branch that reflects the official
releases.

There are also tags and branches that are references in papers and notes
about experiments, such as "fermat-18-c".
